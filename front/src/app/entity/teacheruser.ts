export class TeacherUser {
    id: number;
    email: string;
    pass: string;
    name: string;
    surname: string;
    imgurl: string;
}
