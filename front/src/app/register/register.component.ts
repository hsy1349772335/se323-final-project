import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators,FormGroup } from '@angular/forms';
import { PassErrorStateMatcher } from '../core/errorstatematcher';
import { HttpClient } from '@angular/common/http';
import { StudentService } from '../services/student.service';
import { UserService } from '../services/user.service';
import { User } from '../entity/user';
import { Student } from '../entity/student';
import { StudentUser } from '../entity/studentuser';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
    student: Student;
    students: Student[];
    users: User[];
    regForm: FormGroup;
    fb: FormBuilder = new FormBuilder();
    matcher = new PassErrorStateMatcher();
    private api = environment.regApi;

    constructor(private router: Router,
                private studentSrv: StudentService,
                private userSrv: UserService,
                private http: HttpClient) {
        this.studentSrv.obs().subscribe(students => this.students = students);
        this.userSrv.obs().subscribe(users => this.users = users)
    }

    checkPasswords(group: FormGroup) { 
        let pass = group.controls.password.value;
        let confirmPass = group.controls.confirmPassword.value;
        return pass === confirmPass ? null : { notSame: true }
    }

    ngOnInit(): void {
        this.regForm = this.fb.group({
            stid:               [null, Validators.compose([Validators.required,Validators.pattern('[0-9]{9}')])],
            name:               [null, Validators.required],
            surname:            [null, Validators.required],
            imgurl:              [null, Validators.required],
            dob:                [null, Validators.required],
            password:           [null, Validators.required],
            confirmPassword:    [''],
            email:              [null, Validators.compose([Validators.required,Validators.email])],
        }, {validator: this.checkPasswords }); 
    }

    validation_messages = {
        'stid': [
            { type: 'required', message: 'student id is required'},
            { type:'pattern', message: 'student id must be 9 digits'},
        ],
        'name': [
            { type: 'required', message: 'the name is required' }
        ],
        'surname': [
            { type: 'required', message: 'the surname is required' }
        ],
        'imgurl': [
            { type: 'required', message: 'the image link is required'}
        ],
        'password': [
            { type: 'minLength', message: 'the minmium length is 6' },
            { type: 'required', message: 'the password is required' }, 
            
        ],
        'dob': [
            {type: 'required', message: 'the date of birth is required' }
        ],
        'email': [
            { type: 'required', message: 'email is required' },
            { type: 'email',   message: 'email format is not correct'}
        ],
        'confirmPassword': [
            { type: 'required', message: 'confirm password is required'},
        ]
    };

    register() {
        let fmVal = this.regForm.value;
        let newStu: StudentUser = {
            id: 0,
            email: fmVal.email,
            pass: fmVal.password,
            type: "new-student",
            status: "new",
            stid: fmVal.stid,
            name: fmVal.name,
            surname: fmVal.surname,
            imgurl: fmVal.imgurl,
            dob: fmVal.dob
        }
        this.http.post<any>(this.api, newStu)
            .subscribe(data => {
                this.userSrv.load();
                this.studentSrv.load();
                alert('Registered successfully');
            });
        this.router.navigate(['/login'])
    }
}
