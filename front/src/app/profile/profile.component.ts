import { Component, OnInit } from '@angular/core';
import { Student } from '../entity/student';
import { User } from '../entity/user';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PassErrorStateMatcher } from '../core/errorstatematcher';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { StudentService } from '../services/student.service';
import { UserService } from '../services/user.service';
import { IdService } from '../services/id.service';
import { StudentUser } from '../entity/studentuser';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
    private api = environment.profileApi;
    user: User;
    student: Student = {
        id: 0,
        stid: "",
        name: "",
        surname: "",
        imgurl: "",
        dob: new Date()
    };
    students: Student[];
    pfForm: FormGroup;
    fb: FormBuilder = new FormBuilder();
    matcher = new PassErrorStateMatcher();
    confirmPass = "";

    constructor(private router: Router,
        private studentSrv: StudentService,
        private userSrv: UserService,
        private http: HttpClient,
        private idSrv: IdService) {
        this.pfForm = this.fb.group({
            stid: [null, Validators.compose([Validators.required, Validators.pattern('[0-9]{9}')])],
            name: [null, Validators.required],
            surname: [null, Validators.required],
            imgurl: [null, Validators.required],
            dob: [null, Validators.required],
            password: [null, Validators.required],
            confirmPassword: [''],
            email: [null, Validators.compose([Validators.required, Validators.email])],
        }, { validator: this.checkPasswords });
        this.studentSrv.obs().subscribe(students => {
            this.students = students;
            this.getStudentProfile();
        });
        this.idSrv.obsUser().subscribe(user => {
            this.user = user;
            this.getStudentProfile();
        });
        this.confirmPass = this.user.pass;
    }

    getStudentProfile() {
        if (this.students == null || this.user == null) {
            return;
        }
        let fm = this.pfForm;
        for (let s of this.students) {
            if (s.id == this.user.id) {
                this.student = s;
                fm.get('name').setValue(s.name);
                fm.get('surname').setValue(s.surname);
                fm.get('stid').setValue(s.stid);
                fm.get('imgurl').setValue(s.imgurl);
                fm.get('dob').setValue(s.dob);
                fm.get('email').setValue(this.user.email);
                fm.get('password').setValue(this.user.pass);
                fm.get('confirmPassword').setValue(this.user.pass);
            }
        }
    }

    checkPasswords(group: FormGroup) {
        let pass = group.controls.password.value;
        let confirmPass = group.controls.confirmPassword.value;
        return pass === confirmPass ? null : { notSame: true }
    }

    ngOnInit() { }

    validation_messages = {
        'stid': [
            { type: 'required', message: 'student id is required' },
            { type: 'pattern', message: 'student id must be 9 digits' },
        ],
        'name': [
            { type: 'required', message: 'the name is required' }
        ],
        'surname': [
            { type: 'required', message: 'the surname is required' }
        ],
        'imgurl': [
            { type: 'required', message: 'the image link is required' }
        ],
        'password': [
            { type: 'minLength', message: 'the minmium length is 6' },
            { type: 'required', message: 'the password is required' },
        ],
        'dob': [
            { type: 'required', message: 'the date of birth is required' }
        ],
        'email': [
            { type: 'required', message: 'email is required' },
            { type: 'email', message: 'email format is not correct' }
        ],
        'confirmPassword': [
            { type: 'required', message: 'confirm password is required' },
        ]
    };

    update() {
        let fmVal = this.pfForm.value;
        let newUsr: StudentUser = {
            id: 0,
            email: fmVal.email,
            pass: fmVal.password,
            type: this.user.type,
            status: this.user.status,
            stid: fmVal.stid,
            name: fmVal.name,
            surname: fmVal.surname,
            imgurl: fmVal.imgurl,
            dob: fmVal.dob
        }
        this.http.put<any>(this.api + "/" + this.user.id, newUsr)
            .subscribe(data => {
                this.userSrv.load();
                this.studentSrv.load();
                alert('Update successfully');
            });
        this.router.navigate(['/profile'])
    }

}
