import { Student } from '../entity/student';
import { StudentService } from './student.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StudentApiImplService extends StudentService {
    private students = new BehaviorSubject<Student[]>(null);
    private api = environment.studentApi;

    constructor(private http: HttpClient) {
        super();
        this.load();
    }

    load() {
        this.http.get<Student[]>(this.api)
        .subscribe(students => {
            for (let s of students) this.anonPic(s);
            this.students.next(students);
        });
    }

    obs(): Observable<Student[]> {
        return this.students.asObservable();
    }

    get(id: number): Student {
        if (this.students == null) return null;
        let list = this.students.getValue();
        return list.find((obj) => obj.id === id);
    }

    private nextId(): number {
        let list = this.students.getValue();
        return list.map(obj => obj.id).reduce((a,b) => a > b ? a : b) + 1;
    }

    add(student: Student): Student {
        if (this.students == null) return null;
        let list = this.students.getValue();
        this.anonPic(student);
        list.push(student);
        this.students.next(list);
        this.http.post<any>(this.api, student)
            .subscribe(data => { this.load(); });
        return student;
    }

    del(id: number): Student {
        if (this.students == null) return null;
        let list = this.students.getValue();
        let student = list.find((obj) => obj.id === id);
        if (student === null) return null;
        let i = list.indexOf(student);
        list.splice(i, 1);
        this.students.next(list);
        this.http.delete<any>(this.api + "/" + id)
            .subscribe(data => { this.load(); });
        return student;
    }

    upd(student: Student): Student {
        if (this.students == null) return null;
        let list = this.students.getValue();
        let old = list.find((obj) => obj.id === student.id);
        if (old === null) return null;
        this.anonPic(student);
        let i = list.indexOf(old);
        list[i] = student;
        this.students.next(list);
        this.http.put<any>(this.api + "/" + student.id, student)
            .subscribe(data => { this.load(); });
        return old;
    }

    anonPic(student: Student): Student {
        if (student.imgurl == null || student.imgurl == "") {
            student.imgurl = "assets/anon.png";
        }
        return student;
    }
}

