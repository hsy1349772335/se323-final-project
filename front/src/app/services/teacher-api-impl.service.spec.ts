import { TestBed, inject } from '@angular/core/testing';

import { TeacherApiImplService } from './teacher-api-impl.service';

describe('TeacherApiImplService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TeacherApiImplService]
    });
  });

  it('should be created', inject([TeacherApiImplService], (service: TeacherApiImplService) => {
    expect(service).toBeTruthy();
  }));
});
