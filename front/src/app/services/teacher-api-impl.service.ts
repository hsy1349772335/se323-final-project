import { Teacher } from '../entity/teacher';
import { TeacherService } from './teacher.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TeacherApiImplService extends TeacherService {
    private teachers = new BehaviorSubject<Teacher[]>(null);
    private api = environment.teacherApi;

    constructor(private http: HttpClient) {
        super();
        this.load();
    }

    load() {
        this.http.get<Teacher[]>(this.api)
        .subscribe(teachers => {
            this.teachers.next(teachers);
        });
    }

    obs(): Observable<Teacher[]> {
        return this.teachers.asObservable();
    }

    get(id: number): Teacher {
        if (this.teachers == null) return null;
        let list = this.teachers.getValue();
        return list.find((obj) => obj.id === id);
    }

    private nextId(): number {
        let list = this.teachers.getValue();
        return list.map(obj => obj.id).reduce((a,b) => a > b ? a : b) + 1;
    }

    add(teacher: Teacher): Teacher {
        if (this.teachers == null) return null;
        let list = this.teachers.getValue();
        list.push(teacher);
        this.teachers.next(list);
        this.http.post<any>(this.api, teacher)
            .subscribe(data => { this.load(); });
        return teacher;
    }

    del(id: number): Teacher {
        if (this.teachers == null) return null;
        let list = this.teachers.getValue();
        let teacher = list.find((obj) => obj.id === id);
        if (teacher === null) return null;
        let i = list.indexOf(teacher);
        list.splice(i, 1);
        this.teachers.next(list);
        this.http.delete<any>(this.api + "/" + id)
            .subscribe(data => { this.load(); });
        return teacher;
    }

    upd(teacher: Teacher): Teacher {
        if (this.teachers == null) return null;
        let list = this.teachers.getValue();
        let old = list.find((obj) => obj.id === teacher.id);
        if (old === null) return null;
        let i = list.indexOf(old);
        list[i] = teacher;
        this.teachers.next(list);
        this.http.put<any>(this.api + "/" + teacher.id, teacher)
            .subscribe(data => { this.load(); });
        return old;
    }

}

