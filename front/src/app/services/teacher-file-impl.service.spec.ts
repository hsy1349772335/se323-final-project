import { TestBed, inject } from '@angular/core/testing';

import { TeacherFileImplService } from './teacher-file-impl.service';

describe('TeacherFileImplService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TeacherFileImplService]
    });
  });

  it('should be created', inject([TeacherFileImplService], (service: TeacherFileImplService) => {
    expect(service).toBeTruthy();
  }));
});
