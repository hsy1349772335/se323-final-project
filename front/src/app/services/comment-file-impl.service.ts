import { Injectable } from '@angular/core';
import { Comment } from '../entity/comment';
import { CommentService } from './comment.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CommentFileImplService extends CommentService {
    private comments = new BehaviorSubject<Comment[]>(null);

    constructor(private http: HttpClient) { 
        super();
        this.http.get<Comment[]>('assets/comments.json').subscribe(comments => {
            this.comments.next(comments);
        });
    }

    load() { }

    obs(): Observable<Comment[]> {
        return this.comments.asObservable();
    }

    get(id: number): Comment {
        if (this.comments == null) return null;
        let list = this.comments.getValue();
        return list.find(obj => obj.id == id);
    }

    private nextId(): number {
        let list = this.comments.getValue();
        return list.map(obj => obj.id).reduce((a,b) => a > b ? a : b) + 1;
    }

    add(comment: Comment): Comment {
        if (this.comments == null) return null;
        let list = this.comments.getValue();
        comment.id = this.nextId();
        list.push(comment);
        this.comments.next(list);
        return comment;
    }

    del(id: number): Comment {
        if (this.comments == null) return null;
        let list = this.comments.getValue();
        let comment = list.find(obj => obj.id == id);
        if (comment == null) return null;
        let i = list.indexOf(comment);
        list.splice(i, 1);
        this.comments.next(list);
        return comment;
    }

    upd(comment: Comment): Comment {
        if (this.comments == null) return null;
        let list = this.comments.getValue();
        let old = list.find((obj) => obj.id === comment.id);
        if (old === null) return null;
        let i = list.indexOf(old);
        list[i] = comment;
        this.comments.next(list);
        return old;
    }

}
