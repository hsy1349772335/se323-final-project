import { Observable } from 'rxjs';
import { Activity } from '../entity/activity';

export abstract class ActivityService {
    abstract load();
    abstract obs(): Observable<Activity[]>;
    abstract get(id: number): Activity;
    abstract add(activity: Activity): Activity;
    abstract del(id: number): Activity;
    abstract upd(activity: Activity): Activity;
}

