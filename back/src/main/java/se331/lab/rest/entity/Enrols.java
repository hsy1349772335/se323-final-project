package se331.lab.rest.entity;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Enrols {
  @Id
  @GenericGenerator(name="kaugen" , strategy="increment")
  @GeneratedValue(generator="kaugen")
  @EqualsAndHashCode.Exclude
  Long id;
  int student;
  int activity;
  String status;
}
