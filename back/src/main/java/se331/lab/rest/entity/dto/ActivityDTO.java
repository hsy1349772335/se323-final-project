package se331.lab.rest.entity.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import se331.lab.rest.entity.Activity;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ActivityDTO {
  Long id;
  String desc;
  String loc;
  String name;
  String when;
  String time;
  String regStart;
  String regEnd;
  TeacherDTO lecturer;
  int teacher;

  public static ActivityDTO getActivityListDTO(Activity activity) {
    return ActivityDTO.builder()
      .id(activity.getId())
      .desc(activity.getDesc())
      .loc(activity.getLoc())
      .name(activity.getName())
      .when(activity.getWhen())
      .time(activity.getTime())
      .regStart(activity.getRegStart())
      .regEnd(activity.getRegEnd())
      .teacher(activity.getLecturer().getId().intValue())
      .lecturer(TeacherDTO.getTeacherDTO(activity.getLecturer()))
      .build();

  }

  public Activity getActivity() {
    return Activity.builder()
      .id(this.id)
      .desc(this.getDesc())
      .loc(this.getLoc())
      .name(this.getName())
      .when(this.getWhen())
      .time(this.getTime())
      .regStart(this.getRegStart())
      .regEnd(this.getRegEnd())
      .lecturer(this.lecturer.getTeacher())
      .build();
  }
}
