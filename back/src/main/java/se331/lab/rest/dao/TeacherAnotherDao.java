package se331.lab.rest.dao;

import se331.lab.rest.entity.Teacher;

import java.util.List;

public interface TeacherAnotherDao {
    Teacher getTeacherByLastName(String lastName);
    List<Teacher> getAllTeacher();

    Teacher getTeacherById(Long id);

}
