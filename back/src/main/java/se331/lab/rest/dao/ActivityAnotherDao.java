package se331.lab.rest.dao;

import se331.lab.rest.entity.Activity;

import java.util.List;

public interface ActivityAnotherDao {
    List<Activity> findAll();

    Activity save(Activity activity);
}
