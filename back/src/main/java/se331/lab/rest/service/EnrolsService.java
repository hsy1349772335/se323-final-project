package se331.lab.rest.service;

import se331.lab.rest.entity.Enrols;

import java.util.List;


public interface EnrolsService {
  List<Enrols> getEnrols();
  Enrols save(Enrols enrols);

  Object deleteById(long id);
}
