package se331.lab.rest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import se331.lab.rest.dao.CommentDao;
import se331.lab.rest.entity.Comment;

import java.util.List;

@Service
public class CommentService {
  private final
  CommentDao commentDao;

  @Autowired
  public CommentService(CommentDao commentDao) {
    this.commentDao = commentDao;
  }

  @Transactional

  public List<Comment> getComment() {
    return commentDao.findAll();
  }


  @Transactional
  public Comment save(Comment comment) {
    return commentDao.save(comment);
  }

  public Comment deleteById(long id) {
    return commentDao.deleteById(id);
  }
}
